---
title: "A Collection of Scripts"
date: 2019-08-05T15:17:53-05:00
draft: false
tags: ["shell", "dmenu"]
description: "Just a collection of scripts I use on my machines."
cover: "img/red_highlands.jpg"
---


Prompt
------

    #!/bin/sh
    # A dmenu binary prompt script.
    
    [ "$(printf "No\\nYes" | dmenu -i -p "$1")" = "Yes" ] && $2

I use this ```prompt``` script with ```dmenu``` to create others scripts with a binary prompt. For example I can create a script that ask if I would like to shutdown my machine, if I respond with "Yes" the computer will shutdown.

	#!/bin/sh
	$SCRIPTS/prompt "Do you want to shutdown?" "shutdown -h now"

In order for the above script to work, you need to add ```%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown``` to your ```/etc/sudoers``` file. This allows users in the wheel group to execute ```shutdown``` without a password.
