---
title: "Simple password management with ```dmenu(1)``` and ```keepassxc-cli(1)```"
date: 2019-10-08T12:43:18-05:00
draft: false
tags: ["shell", "dmenu", "keepassxc"]
cover: "img/KeePassXC.jpg"
description: "How to create a KeePassXC prompt with dmenu"
---

As a long time user of KeePassXC, I have always found it a bit cumbersome to have to open KeePassXC, type in my master password, and select proper account just to be able to copy the account's password to my clipboard. I have created a short script that utilizes ```keepassxc-cli(1)``` and ```dmenu(1)``` in order to streamline the process.

	#!/bin/sh
	# Must have keepassxc installed to show menu
	keepassxc-cli -h >/dev/null || exit
	
	# Set variables
	PASSWD=$(dmenu -sb "#282a36" -sf "#bbbbbb" -nf "#44475a" -nb "#44475a" -p "Enter your master password:" <&- && echo)
	KEYFILE="/PATH/TO/KEEPASS/KEYFILE"
	DATABASE="/PATH/TO/KEEPASS/DATABASE"
	
	# Check if PASSWD is correct. If PASSWD is incorrect send a notification and exit the program
	echo "$PASSWD" | keepassxc-cli extract -k $KEYFILE $DATABASE >/dev/null || notify-send "Incorrect master password."
	echo "$PASSWD" | keepassxc-cli extract -k $KEYFILE $DATABASE >/dev/null || exit
	
	# Choose an account
	LOGIN=$(echo "$PASSWD" | keepassxc-cli extract -k $KEYFILE $DATABASE | grep -A 1 Title | grep Value | tr -d '\11' | sed -e "s/<[^>]*>//g" | uniq | xargs -L 1 | dmenu -i -p "Which Account?")
	
	# If account is valid copy the password to the clipboard
	# If the account is invalid send a notification and exit the script
	echo "$PASSWD" | keepassxc-cli clip -k $KEYFILE $DATABASE "$LOGIN" >/dev/null || notify-send "Invalid entry."
	echo "$PASSWD" | keepassxc-cli clip -k $KEYFILE $DATABASE "$LOGIN" >/dev/null || exit
	USERNAME=$( echo "$PASSWD" | keepassxc-cli show -k $KEYFILE $DATABASE "$LOGIN" | grep UserName ) || exit
	notify-send "$USERNAME"
	notify-send " "
	notify-send "Password for $LOGIN copied to clipboard."

Let's break down the logic of the script

  * Check if keepassxc is installed
  * Give the ```$PASSWD``` ```$KEYFILE``` ```$DATABASE``` variables values
  * Check if the ```$PASSWD``` variable is correct
  * Allow the user to select an account
  * Copy corresponding password to the clipboard
  * Notify the user of the account name

